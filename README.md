# Debian Mirror Cloud Ansible

## SSH config

```
Host *.debian.org *.debian.net *.debian.cloud *.debian.invalid
  User $DEBIANUSER

Host *.debian-mirror-v3-prod.azure.debian.invalid
  ProxyJump jump.mirror-azure.infra.debian.cloud

Host *.debian-mirror-v3-staging.azure.debian.invalid
  ProxyJump jump.mirror-azure-staging.infra.debian.cloud
```
